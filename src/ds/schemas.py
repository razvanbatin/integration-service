from marshmallow import Schema, fields


class ProductInfo(Schema):
    id = fields.String(required=True)
    sku = fields.String(required=True)
    name = fields.String(required=True)
    tags = fields.List(fields.Dict(), required=True)
    ts = fields.DateTime(required=True, format='iso')


class ProductStock(Schema):
    id = fields.String(required=True)
    sku = fields.String(required=True)
    stockId = fields.String(required=True)
    quantity = fields.Integer(required=True)
    openPurchaseOrderQuantity = fields.Integer(required=True)
    reservedQuantity = fields.Integer(required=True)
    stockInTransfer = fields.Integer(required=True)
    reorderLevel = fields.Integer(required=True)
    replenishmentQuantity = fields.Integer(required=True)
    averagePurchasePrice = fields.Float(required=True)
    tags = fields.List(fields.Dict(), required=True)
    ts = fields.DateTime(required=True, format='iso')


class ProductPrice(Schema):
    id = fields.String(required=True)
    sku = fields.String(required=True)
    price = fields.Float(required=True)
    channel = fields.String(required=True)
    validFrom = fields.Integer(required=True)
    validTo = fields.Integer(required=True)
    tags = fields.List(fields.Dict(), required=True)
    ts = fields.DateTime(required=True, format='iso')
