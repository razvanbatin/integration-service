import datetime
import json
import logging

from flask import Blueprint, request
from flask_restful import Api, Resource
from ds import config

from ds.schemas import ProductInfo

from src.ds.schemas import ProductStock, ProductPrice

logger = logging.getLogger(__name__)

blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(blueprint)
healthcheck = Blueprint('healthcheck', __name__)


@healthcheck.route('/')
def homepage():
    return f"Data Integration Service API version {config.API_VERSION}", 200


class InfoView(Resource):
    def get(self, product_id):
        return {}

    def post(self):
        if not request.data:
            return 'No data sent!', 400
        product_info = ProductInfo().load(request.json)
        if product_info.errors:
            return product_info.errors, 400
        info = product_info.data
        info['ts'] = datetime.datetime.strftime(
            info['ts'], format="%Y-%m-%d %H:%M:%S")
        logger.info(json.dumps(info))

        return {}, 201


class StockView(Resource):
    def get(self, product_id):
        return {}

    def post(self):
        if not request.data:
            return 'No data sent!', 400
        product_stock = ProductStock().load(request.json)
        if product_stock.errors:
            return product_stock.errors, 400
        stock = product_stock.data
        stock['ts'] = datetime.datetime.strftime(
            stock['ts'], format="%Y-%m-%d %H:%M:%S")
        logger.info(json.dumps(stock))

        return {}, 201


class PriceView(Resource):
    def get(self, product_id):
        return {}

    def post(self):
        if not request.data:
            return 'No data sent!', 400
        product_price = ProductPrice().load(request.json)
        if product_price.errors:
            return product_price.errors, 400
        price = product_price.data
        price['ts'] = datetime.datetime.strftime(
            price['ts'], format="%Y-%m-%d %H:%M:%S")
        logger.info(json.dumps(price))

        return {}, 201


api.add_resource(InfoView, '/product-info', endpoint='product_info')
api.add_resource(StockView, '/product-stock', endpoint='product_stock')
api.add_resource(PriceView, '/product-price', endpoint='product_price')
