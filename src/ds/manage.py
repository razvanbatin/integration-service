import click
from flask.cli import FlaskGroup

from ds.app import create_app


def create_ds(info):
    return create_app(cli=True)


@click.group(cls=FlaskGroup, create_app=create_ds)
def cli():
    """Main entry point"""


@cli.command("init")
def init():
    """
    Init application, create database tables
    """
    from src.ds import db
    click.echo("create database")
    db.create_all()
    click.echo("done")


if __name__ == "__main__":
    cli()
