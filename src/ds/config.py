"""Default configuration

Use env var to override
"""
DEBUG = True
SECRET_KEY = "changeme"

API_VERSION = 'v1'

SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/ds.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'INFO',
    }
}
