from ds import api
from ds import config
from ds.extensions import db, migrate
from flask import Flask
from logging.config import dictConfig

dictConfig(config.LOGGING_CONFIG)


def create_app(config=None, cli=False):
    """Application factory, used to create application
    """
    app = Flask('ds')

    configure_app(app)
    configure_extensions(app, cli)
    register_blueprints(app)

    return app


def configure_app(app):
    """set configuration for application
    """
    # default configuration
    app.config.from_object('ds.config')

    # override with env variable, fail silently if not set
    app.config.from_envvar("DS_CONFIG", silent=True)


def configure_extensions(app, cli):
    """configure flask extensions
    """
    db.init_app(app)

    if cli is True:
        migrate.init_app(app, db)


def register_blueprints(app):
    """register all blueprints for application
    """
    app.register_blueprint(api.views.blueprint)
    app.register_blueprint(api.views.healthcheck)
