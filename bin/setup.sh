#!/usr/bin/env bash

echo
echo "Create local db folder for persisting containerized db"
echo

if [ ! -d "db-data" ];then
    mkdir db-data
else
    echo "db-data directory already created ..."
fi

echo
echo "Setting up pre-commit config ..."
echo

command -v pre-commit  >/dev/null 2>&1

if (( $? == 0 )); then
    echo "pre-commit already installed, skipping ..."
else
    read -p "Command pre-commit not found. Pres Enter to continue installation:"

    command -v pip3 >/dev/null 2>&1
    if (( ! $? == 0 )); then
        echo "Requiring pip3 to proceed. Make sure is installed and accessible."
        echo "Aborting"
        exit 1
    fi
    echo "Installing pre-commit utility ..."
    echo
    pip3 install pre-commit
fi

echo -e "\nInitial run for pre-commit ...\n"
pre-commit install
pre-commit run

echo -e "\nPre-commit installation done!"

echo "Installing pip-tools utility ..."
echo
pip3 install pip-tools

echo "Generating the requirements.txt with the dependencies ..."
echo
pip-compile
