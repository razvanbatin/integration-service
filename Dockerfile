FROM python:3.7.4

RUN apt update && apt install unixodbc unixodbc-dev python3-dev -y

ADD . /cst_dataservice
COPY docker/wsgi.ini /etc/wsgi.ini

WORKDIR /cst_dataservice

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install --no-deps -e .

RUN pip install uwsgi
ENV FLASK_APP=ds.app:create_app
EXPOSE 5000

CMD ["docker/scripts/entrypoint.sh"]
