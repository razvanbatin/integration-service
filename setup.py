from setuptools import setup, find_packages

__version__ = '0.1'


setup(
    name='ds',
    version=__version__,
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    install_requires=[
        'flask',
        'flask-sqlalchemy',
        'flask-restful',
        'flask-migrate',
        'flask-marshmallow',
        'marshmallow-sqlalchemy',
    ],
    entry_points={
        'console_scripts': [
            'ds = ds.manage:cli'
        ]
    }
)
